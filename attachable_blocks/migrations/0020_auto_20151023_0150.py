# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0019_auto_20151022_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachableblock',
            name='block_type',
            field=models.CharField(editable=False, choices=[('actions.actionsblock', 'Actions block type'), ('catalogs.categoriesblock', 'Categories'), ('catalogs.categorydetailblock', 'Category detail'), ('companies.descriptionfullblock', 'Description full'), ('companies.descriptionshortblock', 'Description short'), ('companies.facebookblock', 'Facebook'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('companies.mainoptionsblock', 'Main options'), ('companies.blockmap', 'Maps'), ('companies.menublock', 'Menu'), ('companies.blockourreception', 'Our Reception'), ('client_gallery.ourphotoblock', 'Our photo block '), ('main.mainblocksecond', 'Second block type'), ('client_gallery.sliderblock', 'Slider block '), ('companies.socialblock', 'Social')], verbose_name='block type', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attachablereference',
            name='block_type',
            field=models.CharField(choices=[('actions.actionsblock', 'Actions block type'), ('catalogs.categoriesblock', 'Categories'), ('catalogs.categorydetailblock', 'Category detail'), ('companies.descriptionfullblock', 'Description full'), ('companies.descriptionshortblock', 'Description short'), ('companies.facebookblock', 'Facebook'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('companies.mainoptionsblock', 'Main options'), ('companies.blockmap', 'Maps'), ('companies.menublock', 'Menu'), ('companies.blockourreception', 'Our Reception'), ('client_gallery.ourphotoblock', 'Our photo block '), ('main.mainblocksecond', 'Second block type'), ('client_gallery.sliderblock', 'Slider block '), ('companies.socialblock', 'Social')], verbose_name='block type', max_length=255),
            preserve_default=True,
        ),
    ]
