# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0006_auto_20150918_1403'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachableblock',
            name='block_type',
            field=models.CharField(max_length=255, choices=[('main.mainblockfirst', 'First block type'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')], editable=False, verbose_name='block type'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attachablereference',
            name='block_type',
            field=models.CharField(max_length=255, choices=[('main.mainblockfirst', 'First block type'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')], verbose_name='block type'),
            preserve_default=True,
        ),
    ]
