# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0009_auto_20150924_0246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachableblock',
            name='block_type',
            field=models.CharField(editable=False, choices=[('actions.actionsblock', 'Actions block type'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')], verbose_name='block type', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attachablereference',
            name='block_type',
            field=models.CharField(choices=[('actions.actionsblock', 'Actions block type'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')], verbose_name='block type', max_length=255),
            preserve_default=True,
        ),
    ]
