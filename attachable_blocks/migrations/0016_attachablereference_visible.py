# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0015_auto_20151006_0826'),
    ]

    operations = [
        migrations.AddField(
            model_name='attachablereference',
            name='visible',
            field=models.BooleanField(default=True, verbose_name='visible'),
            preserve_default=True,
        ),
    ]
