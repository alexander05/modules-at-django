# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0016_attachablereference_visible'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachableblock',
            name='block_type',
            field=models.CharField(verbose_name='block type', max_length=255, editable=False, choices=[('actions.actionsblock', 'Actions block type'), ('catalogs.categoriesblock', 'Categories'), ('catalogs.categorydetailblock', 'Category detail'), ('companies.descriptionfullblock', 'Description full'), ('companies.descriptionshortblock', 'Description short'), ('companies.facebookblock', 'Facebook'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('client_gallery.gallerysiteblock', 'Gallery Block'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('client_gallery.ourphotoblock', 'Our photo block '), ('main.mainblocksecond', 'Second block type'), ('client_gallery.sliderblock', 'Slider block '), ('companies.socialblock', 'Social')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attachablereference',
            name='block_type',
            field=models.CharField(verbose_name='block type', max_length=255, choices=[('actions.actionsblock', 'Actions block type'), ('catalogs.categoriesblock', 'Categories'), ('catalogs.categorydetailblock', 'Category detail'), ('companies.descriptionfullblock', 'Description full'), ('companies.descriptionshortblock', 'Description short'), ('companies.facebookblock', 'Facebook'), ('main.mainblockfirst', 'First block type'), ('catalogs.foodmenublock', 'Food menu block type'), ('client_gallery.gallerysiteblock', 'Gallery Block'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('client_gallery.ourphotoblock', 'Our photo block '), ('main.mainblocksecond', 'Second block type'), ('client_gallery.sliderblock', 'Slider block '), ('companies.socialblock', 'Social')]),
            preserve_default=True,
        ),
    ]
