# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0008_auto_20150924_0149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachableblock',
            name='block_type',
            field=models.CharField(editable=False, verbose_name='block type', max_length=255, choices=[('actions.actionsblock', 'Actions block type'), ('main.mainblockfirst', 'First block type'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attachablereference',
            name='block_type',
            field=models.CharField(verbose_name='block type', max_length=255, choices=[('actions.actionsblock', 'Actions block type'), ('main.mainblockfirst', 'First block type'), ('companies.blockmap', 'Maps'), ('companies.blockourreception', 'Our Reception'), ('main.mainblocksecond', 'Second block type')]),
            preserve_default=True,
        ),
    ]
